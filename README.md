YouTube Age Confirmation Bypass
=====
This userscript prevents you from having to sign in to view age restricted videos on YouTube. It does so by loading in the embedded player, which doesn't require age verification. Supports the new Youtube layout.